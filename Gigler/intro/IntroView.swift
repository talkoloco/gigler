//
//  IntroView.swift
//  Gigler
//
//  Created by Ohad on 2015-05-25.
//  Copyright (c) 2015 Gigler. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import QuartzCore

class IntroView : UIView, UIScrollViewDelegate {
    
    var holeView : UIView?
    var circleView : UIView?
    var delegate : IntroProtocol?
    var scrollView : UIScrollView?
    var pageControl : UIPageControl?
    var loginButton : FBSDKLoginButton?
    var appName : UILabel?
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
    
        /*let backgroundImageView = UIImageView(frame: self.frame)
        backgroundImageView.image = UIImage(named: "pic22")
        self.addSubview(backgroundImageView)*/
        
        self.scrollView = UIScrollView(frame: self.frame)
        self.scrollView!.pagingEnabled = true;
        self.addSubview(self.scrollView!)
        
        self.pageControl = UIPageControl(frame: CGRectMake(0, self.frame.size.height*0.8, self.frame.size.width, 10))
        self.pageControl!.currentPageIndicatorTintColor = UIColor(red: 0.153, green:0.533 , blue: 0.796, alpha: 1)
        self.addSubview(self.pageControl!)

        createView("r3", number: 0)
        createView("r2", number: 2)
        createView("r1", number: 1)
        
        //Login Button
        
        /*if (FBSDKAccessToken.currentAccessToken() != nil)
        {
            // User is already logged in, do work such as go to next view controller.
        }
        else
        {*/
            loginButton = FBSDKLoginButton()
            loginButton?.frame = CGRectMake(self.frame.size.width*0.1, self.frame.size.height*0.85, self.frame.size.width*0.8, 60)
            loginButton!.readPermissions = ["public_profile", "email", "user_friends"]
            self.addSubview(self.loginButton!)

        //}
        
        self.appName = UILabel()
        self.appName!.text = "Gigler";
        self.appName!.textColor = UIColor.whiteColor()
        self.appName!.font = UIFont.boldSystemFontOfSize(50)
        self.appName!.sizeToFit()
        self.appName!.center = self.center;
        self.appName!.center.y = self.appName!.center.y - 260
        self.addSubview(self.appName!)
        
        self.pageControl!.numberOfPages = 3;
        self.scrollView!.contentSize = CGSizeMake(self.frame.size.width*3, self.scrollView!.frame.size.height);
        
        //This is the starting point of the ScrollView
        let scrollPoint = CGPointMake(0, 0)
        self.scrollView!.setContentOffset(scrollPoint, animated: true)
        
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func createView(pic : String, number : Int){
        
        let originWidth = self.frame.size.width
        let originHeight = self.frame.size.height
        let view = UIView()
        
        let descriptionLabel = UILabel(frame:CGRectMake(0, self.frame.size.height*0.9, self.frame.size.width*0.8, 60))
        descriptionLabel.textColor = UIColor.whiteColor()
        descriptionLabel.font = UIFont.boldSystemFontOfSize(25)
        descriptionLabel.textAlignment =  .Center;
        descriptionLabel.numberOfLines = 0;

        switch number {
            case 0:
                view.frame =  CGRectMake(originWidth*2, 0, originWidth, originHeight)
                descriptionLabel.text = "Anyone can offer anything"
                descriptionLabel.sizeToFit()
                descriptionLabel.center = self.center
                descriptionLabel.center.y -= 180
            
                let newLayer = CALayer()
                newLayer.bounds = CGRectMake(originWidth*2, 0, originWidth*2, originHeight*2)
                newLayer.backgroundColor = UIColor.blackColor().CGColor
                newLayer.opacity = 0.4 //Adjust as needed
                view.layer.addSublayer(newLayer)
            
            case 1:
                view.frame = self.frame
                descriptionLabel.text = "Welcome to a cheaper world"
                descriptionLabel.sizeToFit()
                descriptionLabel.center = self.center
                descriptionLabel.center.y -= 180
                
                let newLayer = CALayer()
                newLayer.bounds = CGRectMake(0, 0, view.bounds.width*2, view.bounds.height*2)
                newLayer.backgroundColor = UIColor.blackColor().CGColor
                newLayer.opacity = 0.4 //Adjust as needed
                view.layer.addSublayer(newLayer)

            case 2:
                view.frame = CGRectMake(originWidth, 0, originWidth, originHeight)
                descriptionLabel.text = "Browse 1$ offers from people in your area"
                descriptionLabel.sizeToFit()
                descriptionLabel.center = self.center
                descriptionLabel.center.y -= 180
            
                let newLayer = CALayer()
                newLayer.bounds = CGRectMake(0, 0, view.bounds.width*2, view.bounds.height*2)
                newLayer.backgroundColor = UIColor.blackColor().CGColor
                newLayer.opacity = 0.4 //Adjust as needed
                view.layer.addSublayer(newLayer)

            default:
                view.frame = self.frame
                descriptionLabel.text = ""
                descriptionLabel.sizeToFit()
                descriptionLabel.center = self.center
                descriptionLabel.center.y += 150

        }
        
        
        UIGraphicsBeginImageContext(view.frame.size);
        UIImage(named: pic)!.drawInRect(view.bounds)
        let image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        view.backgroundColor = UIColor(patternImage:image)
        view.addSubview(descriptionLabel)


        self.scrollView!.delegate = self;
        self.scrollView!.addSubview(view)
        
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let pageFraction : CGFloat = self.scrollView!.contentOffset.x / CGRectGetWidth(self.bounds)
        self.pageControl!.currentPage = Int(roundf(Float(pageFraction)))

    }
    
  
    
}
