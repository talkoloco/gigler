//
//  IntroDelegate.swift
//  Gigler
//
//  Created by Ohad on 2015-05-25.
//  Copyright (c) 2015 Gigler. All rights reserved.
//

import Foundation

protocol IntroProtocol {
    
    func onDoneButtonPressed()
    
}