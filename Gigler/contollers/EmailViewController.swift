//
//  EmailViewController.swift
//  Gigler
//
//  Created by Ohad on 2015-05-20.
//  Copyright (c) 2015 Gigler. All rights reserved.
//

import Foundation
import UIKit


class EmailViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var barBtn: UIBarButtonItem!
    
    @IBAction func backPressed(sender: UIBarButtonItem) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.navigationController?.navigationBar.barTintColor = UIColor(red: 132/255.0, green:5/255.0, blue: 57/255.0, alpha: 0.5)
        
         //self.navigationController?.navigationBar.layer.borderWidth
        var navigationBar = self.navigationController?.navigationBar
        
        let navBorder = UIView(frame: CGRectMake(0,navigationBar!.frame.size.height-1,navigationBar!.frame.size.width, 1))
        
        navBorder.backgroundColor = UIColor(red: 132/255.0, green:5/255.0, blue: 57/255.0, alpha: 0.5)
        navBorder.opaque = true
        self.navigationController?.navigationBar.addSubview(navBorder)


        
        self.navigationController?.navigationBar.layer.borderColor = UIColor(red: 132/255.0, green:5/255.0, blue: 57/255.0, alpha: 1).CGColor
        
        self.view.userInteractionEnabled == true
        self.title = "Email"
        email.delegate = self
        
        var backImage = UIImage(named: "forward")
        var nextBtn : UIBarButtonItem = UIBarButtonItem(image: backImage, style: UIBarButtonItemStyle.Plain, target: self, action: "next")
        nextBtn.tintColor = UIColor.blackColor()
            
        //nextBtn.setBackButtonBackgroundImage(backImage, forState: .Normal, barMetrics: UIBarMetrics.Default)
        self.navigationItem.rightBarButtonItem = nextBtn
        // Do any additional setup after loading the view, typically from a nib.

        
        //SwipeGesture To Right will close the current view controller.
        let recognizer: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: "closeView")
        recognizer.direction = UISwipeGestureRecognizerDirection.Right
        self.view.addGestureRecognizer(recognizer)
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }
    
    func next(){
        if (!email.text.isEmail()){
            email?.layer.cornerRadius = 5.0
            email?.layer.masksToBounds = true
            email?.layer.borderColor = UIColor.redColor().colorWithAlphaComponent(0.4).CGColor
            email?.layer.borderWidth = 1
            println("invalid")
        } else {
            NSUserDefaults.standardUserDefaults().setObject("true", forKey: "validEmail")
            self.performSegueWithIdentifier("betaSegue", sender: nil)
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        email?.layer.cornerRadius = 5.0
        email?.layer.masksToBounds = true
        email?.layer.borderColor = UIColor.grayColor().colorWithAlphaComponent(0.1).CGColor
        email?.layer.borderWidth = 1
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension String {
    func isEmail() -> Bool {
        let regex = NSRegularExpression(pattern: "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$", options: .CaseInsensitive, error: nil)
        return regex?.firstMatchInString(self, options: nil, range: NSMakeRange(0, count(self))) != nil
    }
}

