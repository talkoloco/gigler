//
//  LoginViewController.swift
//  Gigler
//
//  Created by Ohad on 2015-05-22.
//  Copyright (c) 2015 Gigler. All rights reserved.
//

import Foundation
import UIKit
import FBSDKCoreKit
import FBSDKLoginKit



class LoginViewController: UIViewController, FBSDKLoginButtonDelegate, IntroProtocol {
    
    var loginBtn: FBSDKLoginButton?
    var introView : IntroView?
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        // User is already logged in, do work such as go to next view controller.
        let loggedIn = (FBSDKAccessToken.currentAccessToken() != nil)
        //let introShown = NSUserDefaults.standardUserDefaults().objectForKey("intro_shown") as? String
        if (!loggedIn) {
            introView = IntroView(frame: self.view.frame)
            introView!.delegate = self
            introView!.loginButton!.delegate = self
            introView!.backgroundColor = UIColor.blackColor()
            self.view.addSubview(introView!)
        } else {
            let email =  NSUserDefaults.standardUserDefaults().objectForKey("validEmail") as? String
            if (email != nil) {
                performSegueWithIdentifier("doneBetaSegue", sender: nil)
            } else {
                performSegueWithIdentifier("startSegue", sender: nil)
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func getImageWithColor(color: UIColor, size: CGSize) -> UIImage {
        var rect = CGRectMake(0, 0, size.width, size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        var image: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!){
        println("User Logged In")
        
        if ((error) != nil)
        {
            // Process error
        }
        else if result.isCancelled {
            // Handle cancellations
        }
        else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            if result.grantedPermissions.contains("email") && result.grantedPermissions.contains("public_profile")
            {
                performSegueWithIdentifier("startSegue", sender: nil)
                
            }
        }
        
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!){
        println("User Logged Out")
    }
    
    func returnUserData()
    {
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: nil)
        graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
            
            if ((error) != nil)
            {
                // Process error
                println("Error: \(error)")
            }
            else
            {
                println("fetched user: \(result)")
                let userName : NSString = result.valueForKey("name") as! NSString
                println("User Name is: \(userName)")
                let userEmail : NSString = result.valueForKey("email") as! NSString
                println("User Email is: \(userEmail)")
            }
        })
    }
    
    func onDoneButtonPressed() {
        println("onDoneButtonPressed")
    }
    
}